package com.sumerge;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {
        // write your code here

        List<Employee> Emps = Arrays.asList(
                new Employee("Mohamed","ASE",01134556),
                new Employee("Haitham","SE",01056454),
                new Employee("Ahmed","SE",01256),
                new Employee("Hany","SSE",01254),
                new Employee("Omar","SSE",01057),
                new Employee("Sayed","SSE",011456)
        );

        //Emps.forEach(Emp -> System.out.println(Emp.getname()));
        Map<String, List<Employee>> EmployeeMap = Emps.stream().collect(Collectors.groupingBy(Emp -> Emp.getTitle()));

        EmployeeMap.keySet().forEach((title) -> {

            try{
                int EmpCount = EmployeeMap.get(title).size();
                System.out.println("Title " + title + " Count " + EmpCount);
                EmployeeMap.get(title).forEach(Emp->{
                    System.out.println("Name "+ Emp.getname()+" - Mobile "+Emp.getNumber());
                });
                if(EmpCount<2)
                    throw new ArithmeticException("Employees Holding this title is less than 2 !!!");
            }catch(ArithmeticException e){
                System.out.println(e.getMessage());
            }

        });
    }
}

