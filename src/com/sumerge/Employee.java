package com.sumerge;

public class Employee {
    private String name;
    private String title;
    private int number;

    public String getname(){
        return name;
    }

    public String getTitle(){
        return title;
    }

    public int getNumber(){
        return number;
    }

    Employee(String name,String title,int number){
    this.name=name;
    this.title=title;
    this.number=number;
    }
}
